/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject1;

import java.util.ArrayList;

/**
 *
 * @author Gun
 */
public class Product {

    private int id;
    private String name;
    private double price;
    private String image;

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }

    public static ArrayList<Product> genProductList() {
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1, "late1", 40, "1.jpg"));
        list.add(new Product(2, "late2", 40, "2.jpg"));
        list.add(new Product(3, "late3", 40, "3.jpg"));
        list.add(new Product(4, "late4", 40, "4.jpg"));
        list.add(new Product(5, "late5", 40, "5.jpg"));
        list.add(new Product(6, "late6", 40, "6.jpg"));
        list.add(new Product(7, "late7", 40, "7.jpg"));
        list.add(new Product(8, "late8", 40, "8.jpg"));
        list.add(new Product(9, "late9", 40, "1.jpg"));
        list.add(new Product(10, "late10", 40, "2.jpg"));
        return list;
    }
}
